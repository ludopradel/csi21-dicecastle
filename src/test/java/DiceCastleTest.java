import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DiceCastleTest {

    @Test
    public void hero_start_game_outside_castle() {
        Hero hero = new Hero();
        DiceCastle diceCastle = new DiceCastle(hero);

        assertThat(hero.actualPosition()).isEqualTo(0);
    }

}
